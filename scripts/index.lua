
-- Index
http.register(GET, "/", function(res, req) 
    local user_agent = req.headers["User-Agent"]

    if not user_agent then 
        user_agent = "[ERROR]"
    end

    res:push_template("head", { title = "Hello from lua!" })
    res:push_template("header", {})

    res:push_template("index", {user_agent = user_agent})

    res:push_template("footer", {})
    res:push_template("tail", {})

    res:html()
end)


http.register_static("/public", "/static")
http.register_redirect("/static/favicon.ico", "/favicon.ico")
