
-- Global
http.register(GET, "/global/show", function(res, req) 
    local host = req.headers["Host"]
    if not host then host = "" end

    local var = global_state.get("example")
    if not var then var = "[UNDEFINED]" end


    res:push_template("head", { title = "Hello from lua!" })
    res:push_template("header", {})

    res:push_template("global/index", { host = host, var = var })

    res:push_template("footer", {})
    res:push_template("tail", {})

    res:html()
end)

http.register(GET, "/global/form", function(res, req) 
    local host = req.headers["Host"]
    if not host then host = "" end


    res:push_template("head", { title = "Hello from lua!" })
    res:push_template("header", {})

    res:push_template("global/form", { host = host })

    res:push_template("footer", {})
    res:push_template("tail", {})

    res:html()
end)

http.register(POST, "/global/set", function(res, req) 
    local data = req.post["data"]
    if data then global_state.set("example", data) end

    res:redirect("/global/show")
end)

http.register(GET, "/global/remove", function(res, req) 
    global_state.rem("example")
    res:redirect("/global/show")
end)

http.register_redirect("/global/show", "/global")
