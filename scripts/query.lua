
-- Query
http.register(GET, "/query", function(res, req) 
    local query_html = "<h1>Wartości argumentów:</h1><ul>"
    local has_query = false

    for n, v in pairs(req.query) do
        query_html = query_html .. "<li>" .. html_entities(n) .. " = " .. html_entities(v) .. "</li>"
        has_query = true     
    end

    query_html = query_html .. "</ul>"

    if not has_query then
        query_html = ""
    end


    res:push_template("head", { title = "Hello from lua!" })
    res:push_template("header", {})

    res:push_template("query/index", { vars = query_html })

    res:push_template("footer", {})
    res:push_template("tail", {})

    res:html()
end)
