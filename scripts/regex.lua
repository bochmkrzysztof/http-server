
-- Regex
http.register(GET, "/regex", function(res, req) 
    res:push_template("head", { title = "Hello from lua!" })
    res:push_template("header", {})

    res:push_template("regex/index", {})

    res:push_template("footer", {})
    res:push_template("tail", {})
end)

http.register(GET, "/regex/$var_small;[a-z]+/test", function(res, req) 
    -- Wartości w ścieżce nigdy nie mają wartości 'nil'
    local var = req.path_variables["var_small"]

    res:push_template("head", { title = "Hello from lua!" })
    res:push_template("header", {})

    res:push_template("regex/small", { var = var })

    res:push_template("footer", {})
    res:push_template("tail", {})
end)

http.register(GET, "/regex/$var_big;[A-Z]+/test", function(res, req) 
--                              ^------------ nazwa zmiennej nie może być identyczna
--                                            gdyby była ten handler zostałby dodany
--                                            do powyższej ścieżki a regex zignorowany
    local var = req.path_variables["var_big"]

    res:push_template("head", { title = "Hello from lua!" })
    res:push_template("header", {})

    res:push_template("regex/big", { var = var })

    res:push_template("footer", {})
    res:push_template("tail", {})
     
end)

http.register(GET, "/regex/$var_any/test", function(res, req) 
    local var = req.path_variables["var_any"]

    res:push_template("head", { title = "Hello from lua!" })
    res:push_template("header", {})

    res:push_template("regex/any", { var = var })

    res:push_template("footer", {})
    res:push_template("tail", {})
end)

http.register(GET, "/regex/$var_small/$var_num;[0-9]+", function(res, req) 
--                              ^--------- var_small było już w tym miejscu zadeklarowane więc
--                                         przejmuje po nim regex'a
    local var_1 = req.path_variables["var_small"]
    local var_2 = req.path_variables["var_num"]

    res:push_template("head", { title = "Hello from lua!" })
    res:push_template("header", {})

    res:push_template("regex/nested", { var_1 = var_1, var_2 = var_2 })

    res:push_template("footer", {})
    res:push_template("tail", {})
end)

http.register(GET, "/regex/about/test", function(res, req) 
    res:push_template("head", { title = "Hello from lua!" })
    res:push_template("header", {})

    res:push_template("regex/about", {})

    res:push_template("footer", {})
    res:push_template("tail", {})
end)
