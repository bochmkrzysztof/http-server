
-- Error
http.register_error(400, function(res, req) 
    res:push_template("head", { title = "Błąd żądania" })
    res:push_template("header", {})

    res:push_template("error", { code = "400", description = "Przestań próbować hakować serwer" })

    res:push_template("footer", {})
    res:push_template("tail", {})

    res:html()
end)

http.register_error(404, function(res, req) 
    res:push_template("head", { title = "Nie znaleziono zasobu" })
    res:push_template("header", {})

    res:push_template("error", { code = "404", description = "Nie znaleziono żądanego zasobu" })

    res:push_template("footer", {})
    res:push_template("tail", {})

    res:html()
end)

http.register_error(500, function(res, req) 
    res:push_template("head", { title = "Wewnętrzny błąd serwera" })
    res:push_template("header", {})

    res:push_template("error", { code = "500", description = "Coś dziwnego stało się wewnątrz serwera" })

    res:push_template("footer", {})
    res:push_template("tail", {})

    res:html()
end)
