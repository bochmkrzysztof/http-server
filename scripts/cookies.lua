
-- Cookies
http.register(GET, "/cookies", function(res, req) 
    local host = req.headers["Host"]
    if not host then host = "" end

    local cookies_html = "<ul>"
    local has_cookies = false

    for k, v in pairs(req.cookies) do 
        cookies_html = cookies_html .. "<li>" .. k .. " = " .. v .. "</li>" 
        has_cookies = true
    end

    if has_cookies then
        cookies_html = cookies_html .. "</ul>"
    else
        cookies_html = "<b>[EMPTY]</b>"
    end



    res:push_template("head", { title = "Hello from lua!" })
    res:push_template("header", {})

    res:push_template("cookies/index", { cookies = cookies_html, host = host })

    res:push_template("footer", {})
    res:push_template("tail", {})
end)

http.register(GET, "/cookies/add", function(res, req) 
    res:set_cookie("foo_1", "bar_1")
    res:set_cookie("foo_2", "bar_2")
    res:set_cookie("foo_3", "bar_3")
    res:set_cookie("foo_4", "bar_4")

    res:redirect("/cookies")
end)

http.register(GET, "/cookies/remove", function(res, req) 
    for k, _ in pairs(req.cookies) do 
        res:remove_cookie(k)
    end
    
    res:redirect("/cookies")
end)
