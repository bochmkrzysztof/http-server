from alpine

# basic setup
run echo http://dl-3.alpinelinux.org/alpine/edge/main >> /etc/apk/repositories &&\
    apk update &&\
    apk add lua5.4 liburing wget libstdc++&&\
#download server\
    wget https://gitlab.com/bochmkrzysztof/http-server/uploads/4b90db76e37b521c0b6e3f751bb276fb/x86_64-alpine-linux &&\
    mkdir -p /usr/local/bin &&\
    install x86_64-alpine-linux /usr/local/bin/kb-http-server &&\
    rm x86_64-alpine-linux &&\
# example server\
    apk add git&&\
    git clone https://gitlab.com/bochmkrzysztof/http-server --branch v1.0.2&&\
    apk del git&&\
    mkdir -p server&&\
    cd server &&\
    mv /http-server/scripts .&&\
    mv /http-server/templates .&&\
    mv /http-server/public .

workdir server
expose 8080
cmd kb-http-server
