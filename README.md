# Simple http server

This is a simple http server scripted in lua

## Directory structure

The server loads lua scripts from firectory ./scripts/
and templates from ./templates/ . Scripts directory
must at least exists, however you may delete templates
directory.

## Basic example

```lua
http.register(GET, "/", function(res, req) 
    res:pushln("Hello, world!")
    res:pushln(req.headers["User-Agent"])
end)
```

complete list of functions is present in attached
example page

# Building

## Linux

To build this server you will need any c++20 compatible
compiler, cmake, boost, lua and uring, then you can simply
run:

```sh
mkdir build
cd build
cmake ..
make
```

and to install

```sh
sudo make install
```

For arch based distros:

```sh
yay -S kb-simple-http-server-git
```

## BSD and MacOS

Unfortunetely current version of asio
does not support kqueue for reading files
therefor it is not possible to build this server
for these platforms
