#ifndef __HTTP_ESCAPE_H
#define __HTTP_ESCAPE_H


#include <string_view>
#include <string>


namespace http::escape {
    auto encode(std::string_view) -> std::string;
    auto decode(std::string_view) -> std::string;
}


#endif
