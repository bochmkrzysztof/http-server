#include "escape.h"
#include "fmt/core.h"

#include <ranges>
#include <string_view>


using namespace http;


__attribute__((warning ("This function is not yet tested")))
auto escape::encode(std::string_view string) -> std::string {
    std::string res;

    for(char ch : string) {
        if(isalnum(ch) || (ch == '-' || ch == '_' || ch == '.' || ch == '~')) {
            res += ch; 
        } else {
            res += fmt::format("%{:2X}", ch) ;
        }
    }

    return res;
}

auto escape::decode(std::string_view string) -> std::string {
    std::string res;

    for(std::string_view::iterator it = string.begin(); it != string.end(); it++) {
        if(*it == '%' && std::distance(it, string.end()) > 2) {
            char first = *++it;
            char second = *++it;

            std::string_view hex = "0123456789ABCDEF";

            auto pos_1 = hex.find(first); 
            auto pos_2 = hex.find(second); 

            if(pos_1 == std::string_view::npos || pos_2 == std::string_view::npos) {
                res += '%'; 
                res += first; 
                res += second; 
            } else {
                char ch = (pos_1 << 4) | pos_2;
                res += ch;
            }
        } else if (*it == '+') {
            res += ' ';
        } else res += *it;
    }

    return res;
}
