#include "connection.h"

#include "basic_serwer/asio_context.h"
#include "basic_serwer/basic_async_server.h"
#include "basic_serwer/serwer_thread.h"

#include "request.h"
#include "response.h"

#include "scripts/path_manager.h"

#include <boost/asio.hpp>

#include <cassert>
#include <cstring>

#include <fmt/core.h>
#include <fmt/color.h>

#include <memory>
#include <charconv>

#include "util/logger.h"


using namespace http;



Connection::Connection(tcp::socket&& socket)
    :socket(std::move(socket)), buffer(0x1000000),
    strand(AsioContext::get_instance().get_ctx()),
    timer(AsioContext::get_instance().get_ctx())
{}

Connection::~Connection() {
    timer.cancel();
}

void Connection::start() {
    async_read_line();   
} 

void Connection::setup_timeout() {
    using namespace std::chrono_literals;

    timer.expires_after(10s);

    timer.async_wait(io::bind_executor(strand, [weak_self = weak_from_this()](error_code error) {
        if(!error) {
            if(auto self = weak_self.lock()) {
                self->socket.cancel();
            } else {
                logger::error("Timer expired after connection was destroyed");
            }
        }
    }));
}

auto Connection::read_data_from_buffer(size_t len) -> std::string {
    auto data = buffer.data();
    auto begin = io::buffers_begin(data);
    auto end = begin + len;

    auto str = std::string(begin, end);
    buffer.consume(len);

    return str;
}

void Connection::check_read(error_code error, std::function<void()> fn) {
    if(!error) {
        fn();
    } else if(error == io::error::eof) {
        // NOP
    } else if(error == io::error::operation_aborted) {
        // NOP
    } else {
        log_read_error(error);
        bad_request();
    }
}

void Connection::async_read_line() {
    setup_timeout();

    io::async_read_until(socket, buffer, '\n', io::bind_executor(strand, [self = shared_from_this()] (error_code error, size_t len) {
        self->check_read(error, [self, len] {
            auto line = self->read_data_from_buffer(len);

            std::string_view line_view = line;
            line_view.remove_suffix(2);     
        
            auto request = Request::from_line(line_view);

            if(request) {
                self->current_request = request;
                self->async_read_headers();
            } else self->bad_request();
        });
    })); 
}

void Connection::async_read_headers() {
    io::async_read_until(socket, buffer, '\n', io::bind_executor(strand, [self = shared_from_this()] (error_code error, size_t len) {
        self->check_read(error, [self, len] {
            auto line = self->read_data_from_buffer(len);

            std::string_view line_view = line;
            line_view.remove_suffix(2);     

            assert(self->current_request); 

            if(line_view.empty()) {
                auto len = self->current_request.value().content_length();
                if(len && len.value() > 0) {
                    self->async_read_content();
                } else {
                    self->good_request(std::move(self->current_request.value()));
                }
            } else if(self->current_request.value().add_header(line_view)) {
                self->async_read_headers();
            } else {                                                
                self->bad_request();
            }
        });
    })); 
}

// Match condition ( for some reason lambda does not compile )
    using iterator = boost::asio::buffers_iterator<boost::asio::streambuf::const_buffers_type>;

    class match_n {
    private:
        size_t n;
        size_t read;
    public:
        explicit match_n(size_t n) : n(n), read(0) {}
        auto operator() (iterator begin, iterator end) -> std::pair<iterator, bool> {
            iterator i = begin;
            while (i != end) {
                i++;
                if (++read == n)
                    return std::make_pair(i, true);
            }
            return std::make_pair(i, false);
        }
    };

    template <> struct boost::asio::is_match_condition<match_n> : public boost::true_type {};


void Connection::async_read_content() {
    assert(current_request);
    auto len_op = current_request.value().content_length();

    assert(len_op);
    auto len = len_op.value();

    if(len > 0x1000000) {
        should_close = true;
        send_response(threads::WorkerThread::get_thread_response_manager().handle_error_and_headers(413));
        return;
    }

    io::async_read_until(socket, buffer, match_n(len), io::bind_executor(strand, [self = shared_from_this()] (error_code error, size_t len) {   
        self->check_read(error, [self, len] {
            auto content = self->read_data_from_buffer(len);
            self->current_request.value().set_content(std::move(content));
            self->good_request(std::move(self->current_request.value()));
        });
    })); 

}

void Connection::good_request(Request request) {
    auto req = std::make_shared<Request>(std::move(request));
    log_request_received(req);

    send_response(threads::WorkerThread::get_thread_response_manager().handle_request_and_headers(*req));
    req.reset();
}

void Connection::bad_request() {
    should_close = true;

    log_bad_request_error();
    send_response(threads::WorkerThread::get_thread_response_manager().handle_error_and_headers(400));
}

void Connection::send_response(Response response) {
    auto res = std::make_shared<Response>(std::move(response));

    log_response_sending(res);
    Connection::response = std::move(res);

    async_write_headers();
}

void Connection::async_write_headers() {
    timer.cancel();

    if(response) { 
        const auto& res = response.value();
        auto headers = std::make_shared<std::string>(res->header_string());

        io::async_write(socket, io::buffer(*headers), io::bind_executor(strand, [self = shared_from_this(), headers](error_code error, size_t) {
            if(!error) {
               self->async_write_data();
            } else self->log_send_error(error);
        }));
    }
}

void Connection::async_write_data() {
    if(response) { 
        const auto& res = response.value(); 

        auto data = res->get_data();
        if(!data) {
            async_read_line();
            return;   
        }

        auto streambuf = std::make_shared<io::streambuf>(0x1000000);
        auto& d = data.value();

        d->get_data(*streambuf, io::bind_executor(strand, [self = shared_from_this(), streambuf](error_code error, size_t len) {
            if(len && (!error || error == io::error::eof)) {
                io::async_write(self->socket, streambuf->data(), io::bind_executor(self->strand, 
                            [self, streambuf, read_error = error](error_code error, size_t) {

                    if(!error) {
                        if(read_error != io::error::eof) { 
                            self->async_write_data();
                        } else if(!self->should_close) {
                            self->async_read_line();
                        }
                    } else self->log_send_error(error);
                }));
            }

            if(error && error != io::error::eof) self->log_data_error(error);
        }));
    } 
}




void Connection::log_read_error(error_code error) {
    logger::warning(fmt::format("Failed to receive data from client: {}", error.what()));
}

void Connection::log_send_error(error_code error) {
    logger::warning(fmt::format("Failed to send data to client: {}", error.what()));
}

void Connection::log_data_error(error_code error) {
    logger::error(fmt::format("Unhandled error while trying to acquire data to be sent to client: {}\n", error.what()));
}

void Connection::log_bad_request_error() {
    logger::info("Received bad request");
}

void Connection::log_request_received(std::shared_ptr<Request> request) {
    logger::info(fmt::format("Received request: {:q}", *request));
}

void Connection::log_response_sending(std::shared_ptr<Response> response) {
    logger::info(fmt::format("Sending response: {:}", *response));
}
