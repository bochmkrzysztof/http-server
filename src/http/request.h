#ifndef __HTTP_REQUEST_H
#define __HTTP_REQUEST_H


#include <string>
#include <string_view>
#include <unordered_map>

#include <optional>

#include <fmt/color.h>
#include <fmt/format.h>


namespace http {
    enum class RequestMethod {
        GET,
        POST 
    };

    typedef std::unordered_map<std::string, std::string> StringMap;
    typedef std::unordered_map<std::string_view, std::string_view> ViewMap;

    class Request {
    private:
        RequestMethod method;
        std::string path;

        StringMap query;
        StringMap headers; 
        StringMap path_variables;
        std::optional<StringMap> post_params;

        // cookies are ownde by one of the headers so no ownership needed here
        std::optional<ViewMap> cookies;

        std::optional<std::string> content;

    public:
        Request(RequestMethod method, std::string path, std::unordered_map<std::string, std::string> query);

        auto add_header(std::string_view line) -> bool;
        void add_query_variable(std::string name, std::string value);
        
        static auto from_line(std::string_view line) -> std::optional<Request>;

        auto get_method() const { return method; }
        auto get_path() const -> std::string_view { return path; }

        auto get_query() const -> const StringMap& { return query; }
        auto get_headers() const -> const StringMap& { return headers; };
        auto get_path_variables() const -> const StringMap& { return path_variables; }
        auto get_or_parse_cookies() -> const ViewMap&;
        auto get_or_parse_post_params() -> const StringMap&;

        auto get_header(const std::string& name) const -> const std::optional<std::string_view>;
        auto content_length() const -> std::optional<size_t>;

        auto set_path_variables(StringMap&& map) { path_variables = map; }

        auto get_content() const -> std::optional<std::string_view> { return content; }
        void set_content(std::string&& data) { content = data; }

    private:
        void parse_cookies();       
        void parse_post_params();

        static auto parse_path(std::string_view path) -> std::optional<std::pair<std::string, StringMap>>;
        static auto parse_url_params(std::string_view params) -> std::optional<StringMap>;
    };   
};





template <> struct fmt::formatter<http::RequestMethod>: formatter<std::string_view> {
    template <typename FormatContext>
    auto format(http::RequestMethod m, FormatContext& ctx) {
        std::string_view name = "unknown";
        switch (m) {
            case http::RequestMethod::GET:  name = "GET";   break;
            case http::RequestMethod::POST: name = "POST";  break;
        }
        return fmt::formatter<std::string_view>::format(name, ctx);
    }
};

template <> struct fmt::formatter<http::Request> {
    // NOTE: when using 'q' values and names are not re-escaped this
    // might result in ambigious or invalid url

    bool disp_headers = false;
    bool disp_query_compact = false;
    bool disp_query = false;

    constexpr auto parse(format_parse_context& ctx) -> decltype(ctx.begin()) {
        auto it = ctx.begin(), end = ctx.end();
        
        while(it != end) {
            switch(*it) {
                case 'h': disp_headers = true; break; 
                case 'q': disp_query = false; disp_query_compact = true; break;
                case 'Q': disp_query = true; disp_query_compact = false; break;
                case '}': return it;
                default: throw format_error("Invalid quelifier");
            }
            it++;
        }

        return it;
    }

    template <typename FormatContext>
    auto format(const http::Request& r, FormatContext& ctx) -> decltype(ctx.out()) {
        auto out = ctx.out();
        out = format_to(out, "{} path: {}", r.get_method(), r.get_path());

        auto q = r.get_query();
        auto h = r.get_headers();
       
        if(disp_query_compact && !q.empty()) {
            bool first = true;
            out = format_to(out, "?"); 
            for(const auto& [name, value] : q) {
                if(!first) out = format_to(out, "&");
                out = format_to(out, "{}={}", name, value);
                first = false; 
            } 
        } 
        out = format_to(out, "\n");

        if(disp_query && !q.empty()) {
            out = format_to(out, "Query:\n");
            for(const auto& [name, value] : q)
                out = format_to(out, "{}: {}\n", name, value); 
        }

        if(disp_headers && !h.empty()) {
            out = format_to(out, "Headers:\n");
            for(const auto& [name, value] : h)
                out = format_to(out, "{}: {}\n", name, value); 
        }
        return out;
    }
};


#endif
