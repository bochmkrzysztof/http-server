#ifndef __HTTP_SERVER_H
#define __HTTP_SERVER_H

#include "basic_serwer/basic_async_server.h"

#include "scripts/path_manager.h"


namespace http {
    class Server: public basic_server::Server {
    private:

    public:
        Server(uint16_t port);

        void log_accept_error(error_code&& error);
        void log_server_start(uint16_t port);
    };  
};


#endif
