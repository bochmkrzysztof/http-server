#ifndef __HTTP_CONNECTION_H
#define __HTTP_CONNECTION_H

#include "basic_serwer/basic_async_server.h"

#include "response.h"
#include "request.h"

#include "scripts/path_manager.h"

#include <functional>
#include <memory>
#include <optional>


namespace http {
    class Connection: public std::enable_shared_from_this<Connection> {
    private: 
        tcp::socket socket;
        io::streambuf buffer;

        io::io_context::strand strand;
        io::high_resolution_timer timer;

        std::optional<Request> current_request;
        std::optional<std::shared_ptr<Response>> response;

        bool should_close = false;

    public:
        Connection(tcp::socket&& socket);
        ~Connection();

        void start();

    private:
        void good_request(Request request);
        void bad_request();

        void send_response(Response response);

        void setup_timeout();
        auto read_data_from_buffer(size_t len) -> std::string;

        void check_read(error_code error, std::function<void()> fn);

        // State machine begin
        void async_read_line();
        void async_read_headers();
        void async_read_content();

        void async_write_headers();
        void async_write_data();
        // State machine end

        void log_read_error(error_code error);
        void log_send_error(error_code error);
        void log_data_error(error_code error);
        void log_bad_request_error();

        void log_request_received(std::shared_ptr<Request> request);
        void log_response_sending(std::shared_ptr<Response> response);
    };  
};


#endif
