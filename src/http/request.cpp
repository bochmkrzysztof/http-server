#include "request.h"

#include "escape.h"

#include <cassert>
#include <iterator>
#include <optional>
#include <regex>

#include <fmt/core.h>
#include <string>
#include <string_view>
#include <unordered_map>
#include <utility>
#include <charconv>

using namespace http;


Request::Request(RequestMethod method, std::string path, std::unordered_map<std::string, std::string> query)
    :method(method), path(std::move(path)), query(std::move(query))
{}

bool Request::add_header(std::string_view line) {
    auto name_end = line.find(": ");
    auto name = line.substr(0, name_end); 
    auto value = line.substr(name_end + 2);

    headers[std::string(name)] = std::string(value);

    return true;
}

void Request::add_query_variable(std::string name, std::string value) {
    query[std::move(name)] = std::move(value); 
}

auto Request::get_header(const std::string& name) const -> const std::optional<std::string_view> {
    auto header = headers.find(name);
    if(header != headers.end()) {
        return header->second;     
    } else return std::nullopt;
}

auto Request::content_length() const -> std::optional<size_t> {
    auto content_length = get_header("Content-Length");
    if(content_length) {
        size_t len;

        auto view = content_length.value();
        auto [_, ec] = std::from_chars(view.data(), view.data() + view.size(), len);

        if(ec == std::errc()) {
            return len;
        }
    }

    return std::nullopt;
}

std::optional<Request> Request::from_line(std::string_view line) {
    auto method_end = line.find_first_of(' '); 
    auto method = line.substr(0, method_end);

    if(method_end == std::string_view::npos) return std::nullopt;

    line.remove_prefix(method_end + 1);

    auto path_end = line.find_first_of(' ');
    auto path = line.substr(0, path_end);

    if(path_end == std::string_view::npos) return std::nullopt;

    auto parsed_path = parse_path(path);
    if(parsed_path) {
        auto [path_str, query] = std::move(parsed_path.value());

        RequestMethod m;
        if(method == "GET") {
            m = RequestMethod::GET;
        } else if(method == "POST") {
            m = RequestMethod::POST;
        } else return std::nullopt;

        return Request(m, std::move(path_str), std::move(query));
    } else return std::nullopt;
}

auto Request::parse_path(std::string_view path) -> std::optional<std::pair<std::string, std::unordered_map<std::string, std::string>>> {
    std::unordered_map<std::string, std::string> query;

    auto param_end = path.find('?');
    std::string path_str(path.substr(0, param_end));

    if(param_end != std::string_view::npos) {
        path.remove_prefix(param_end + 1);         
        
        auto map = parse_url_params(path);
        if(map) {
            query = std::move(map.value());
        } else return std::nullopt;
    } 

    return std::make_pair(path_str, query);
}

auto Request::get_or_parse_post_params() -> const std::unordered_map<std::string, std::string>& {
    if(!post_params) parse_post_params();

    assert(post_params);
    return post_params.value();
}

auto Request::get_or_parse_cookies() -> const std::unordered_map<std::string_view, std::string_view>& {
    if(!cookies) parse_cookies();

    // after parse_cookies() cookies are garenteed to be set
    // even if there is no Cookies header
    assert(cookies);
    return cookies.value();
}

void Request::parse_post_params() {
    StringMap params;

    auto content_type = headers.find("Content-Type");
    if(content_type != headers.end() && content_type->second == "application/x-www-form-urlencoded" && content) {
        auto map = parse_url_params(content.value()); 
        if(map) params = std::move(map.value()); 
    }

    post_params = params;
    content.reset();
}

void Request::parse_cookies() {
    ViewMap map;

    auto result = headers.find("Cookie");
    if(result != headers.end()) {
        std::string_view header = result->second;

        auto sep = std::string_view::npos;

        do {
            sep = header.find("; ");

            auto cookie = header.substr(0, sep);
            header.remove_prefix(sep + 2);

            auto eq = cookie.find('=');
            if(eq == std::string_view::npos) break;
            
            auto name = cookie.substr(0, eq);
            auto value = cookie.substr(eq + 1);

            map[name] = value;
        } while(sep != std::string_view::npos);
    }

    cookies = map;
} 

auto Request::parse_url_params(std::string_view params) -> std::optional<StringMap> {
    StringMap map;

    auto param_end = params.find('&');

    do {
        param_end = params.find('&');

        auto param = params.substr(0, param_end);
        params.remove_prefix(param_end + 1);

        auto name_end = param.find('='); 
        if(name_end == std::string_view::npos) return std::nullopt;

        auto name = param.substr(0, name_end);
        auto value = param.substr(name_end + 1);

        auto name_decoded = http::escape::decode(name);
        auto value_decoded = http::escape::decode(value);

        map[std::move(name_decoded)] = std::move(value_decoded);
    } while(param_end != std::string_view::npos);

    return map;
}
