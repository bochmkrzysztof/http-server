#ifndef __HTTP_RESPONSE_H
#define __HTTP_RESPONSE_H


#include <boost/asio/buffer.hpp>
#include <cstdint>

#include <optional>
#include <memory>
#include <string>
#include <string_view>
#include <unordered_map>

#include <fmt/core.h>
#include <fmt/format.h>

#include "response_data/response_data.h"


namespace http {
    class ResponseStatus {
        uint16_t code;
        
    public:
        ResponseStatus(uint16_t code) :code(code) {};

        auto get_code() const { return code; }
        auto get_name() const -> std::string_view; 
    };


    class Response {
        ResponseStatus status; 

        std::unordered_multimap<std::string, std::string> headers;

        std::optional<std::unique_ptr<ResponseData>> data;

    public:
        Response(ResponseStatus status);
        Response(ResponseStatus status, std::string data);

        bool set_data(std::string d);
        bool append_data(std::string d);

        bool send_file(std::string filen);

        void set_status(uint16_t c);
        auto get_status() const -> const ResponseStatus& { return status; }

        void add_header(std::string k, std::string v);
        auto get_headers() const -> const std::unordered_multimap<std::string, std::string>& { return headers; }

        auto header_string() const -> std::string;
        auto get_data() const -> std::optional<ResponseData*>;

        void calculate_length();
    };
};

template <> struct fmt::formatter<http::ResponseStatus>: formatter<std::string_view> {
    template <typename FormatContext>
    auto format(http::ResponseStatus m, FormatContext& ctx) {
        auto name = m.get_name();
        return fmt::formatter<std::string_view>::format(name, ctx);
    }
};

template <> struct fmt::formatter<http::Response> {
    bool disp_headers = false;

    constexpr auto parse(format_parse_context& ctx) -> decltype(ctx.begin()) {
        auto it = ctx.begin(), end = ctx.end();
        if(it != end && *it == 'h') { disp_headers = true; it++; }
        if(it != end && *it != '}') throw format_error("invalid format");
        return it;
    }

    template <typename FormatContext>
    auto format(const http::Response& r, FormatContext& ctx) -> decltype(ctx.out()) {
        auto out = ctx.out();
        out = format_to(out, "Status: {}\n", r.get_status());
        auto& headers = r.get_headers();
        if(disp_headers && !headers.empty()) {
            out = format_to(out, "Headers:\n");
            for(const auto& [name, value] : headers)
                out = format_to(out, "{}: {}\n", name, value); 
        }
        return out;
    }
};

#endif
