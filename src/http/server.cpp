#include "server.h"

#include <fmt/core.h>
#include <fmt/color.h>
#include <memory>

#include "basic_serwer/basic_async_server.h"
#include "scripts/path_manager.h"
#include "util/logger.h"

#include "connection.h"


using namespace http;



Server::Server(uint16_t port)
    :basic_server::Server(port, 
        [&](tcp::socket sock){ std::make_shared<Connection>(std::move(sock))->start(); }, 
        [&](error_code error){ log_accept_error(std::move(error)); })
{
    async_accept();
    log_server_start(port);
}

void Server::log_accept_error(error_code&& error) {
    logger::error(fmt::format("Accept error occured: {}", error.what()));
}

void Server::log_server_start(uint16_t port) {
    logger::info(fmt::format("Started http server at {}:{}", "http://localhost", port));
}
