#include "response.h"

#include <boost/asio/buffer.hpp>
#include <fmt/core.h>
#include <memory>
#include <string_view>

#include "response_data/file_data.h"
#include "response_data/response_data.h"
#include "response_data/string_data.h"

#include <stdexcept>

using namespace http;


auto ResponseStatus::get_name() const -> std::string_view {
    std::string_view name = "Unknown";

    switch(code) {
        case 200: name = "OK"; break;
        case 201: name = "Created"; break;
        case 202: name = "Accepted"; break;
        case 204: name = "No Content"; break;
        case 205: name = "Reset Content"; break;
        
        case 300: name = "Multiple Choice"; break;
        case 301: name = "Moved Permamently"; break;
        case 302: name = "Found"; break;
        case 303: name = "See Other"; break;
        
        case 400: name = "Bad Request"; break;
        case 403: name = "Forbidden"; break;
        case 404: name = "Not Found"; break;
        case 405: name = "Method Not Allowed"; break;
        case 408: name = "Request Timeout"; break;
        case 410: name = "Gone"; break;
        case 413: name = "Payload Too Large"; break;
        case 414: name = "URI Too Long"; break;
        case 418: name = "I'm a teapot"; break;

        case 500: name = "Internal Server Error"; break;
        case 501: name = "Not Implemented"; break;
    }

    return name;
}

Response::Response(ResponseStatus status)
    :status(status) {}

Response::Response(ResponseStatus status, std::string data)
    :status(status), data(std::make_unique<data::StringData>(data)) {}

bool Response::set_data(std::string d) {
    if(data) return false;

    data = std::make_unique<data::StringData>(std::move(d)); 
    return true;
}

bool Response::append_data(std::string d) {
    if(data) 
        return data.value()->append_data(std::move(d));
    else return set_data(std::move(d));
}

bool Response::send_file(std::string filen) {
    if(data) return false;

    data = std::make_unique<data::FileData>(std::move(filen));
    return true;
}

void Response::set_status(uint16_t c) {
    status = ResponseStatus(c);
}

void Response::add_header(std::string k, std::string v) {
    headers.emplace(std::move(k), std::move(v));
}

auto Response::header_string() const -> std::string {
    std::string s = fmt::format("HTTP/1.1 {} {}\r\n", status.get_code(), status.get_name());

    for(const auto& [name, value] : headers)
        s += fmt::format("{}: {}\r\n", name, value);

    s += "\r\n";
    return s;
}

auto Response::get_data() const -> std::optional<ResponseData*> {
    if(data)
        return data.value().get();
    else return std::nullopt;
}

void Response::calculate_length() {
    if(!headers.contains("Content-Length")) {
        if(data) {
            headers.emplace("Content-Length", fmt::format("{}", data.value()->get_length()));
        } else {
            headers.emplace("Content-Length", "0");
        } 
    }
}
