#ifndef __TEMPLATES_TEMPLATE_MANAGER_H
#define __TEMPLATES_TEMPLATE_MANAGER_H


#include <string>
#include <unordered_map>
#include <optional>

#include "util/singleton.h"
#include "template.h"


namespace templates {
    class TemplateManager : public Singleton<TemplateManager> {
    private:
        friend class Singleton<TemplateManager>; 

        std::unordered_map<std::string, Template> templates;

        TemplateManager() = default;

    public:
        auto render_template(const std::string& name, const std::unordered_map<std::string, std::string>& variables) const -> 
            std::optional<std::string>;

        bool load_templates();

    private:
        void log_template_load(std::string&& name);
        void log_template_load_error(std::string&& name);
    };
}


#endif
