#include "template.h"

#include "util/html_encode.h"

#include <bits/ranges_algo.h>
#include <cctype>
#include <fstream>
#include <memory>
#include <string>
#include <algorithm>

#include <fmt/core.h>
#include <fmt/color.h>

#include "util/logger.h"


using namespace templates;


auto UnescapedTemplatePart::render(const std::unordered_map<std::string, std::string>& vars) const -> std::optional<std::string> {
    auto var = vars.find(name);
    if(var != vars.end()) {
        return var->second; 
    } else return std::nullopt;
}

auto EscapedTemplatePart::render(const std::unordered_map<std::string, std::string>& vars) const -> std::optional<std::string> {
    auto var = vars.find(name);
    if(var != vars.end()) {
        return html_encode(var->second); 
    } else return std::nullopt;
}

Template::Template(std::vector<std::unique_ptr<TemplatePart>>&& parts) {
    this->parts.swap(parts); 
}

auto Template::create(std::filesystem::path&& path) -> std::optional<Template> {
    auto source = read_file(std::move(path));
    if(!source) return std::nullopt;

    std::vector<std::unique_ptr<TemplatePart>> parts;
    std::string_view source_view = source.value();

    while(source_view.length() > 0) {
        auto var_pos = source_view.find("{{");
        if(var_pos > 0 || var_pos == std::string_view::npos) {
            auto string_part = source_view.substr(0, var_pos);
            parts.emplace_back(std::make_unique<StringTemplatePart>(std::string(string_part)));
            source_view.remove_prefix(std::min(var_pos, source_view.length()));
        } else {
            // find and check end of variable
            auto var_end_pos = source_view.find("}}");
            if(var_end_pos == std::string_view::npos) {
                log_unfinished_variable_error();
                return std::nullopt;
            }

            auto var_view = source_view.substr(2, var_end_pos - 2);
            source_view.remove_prefix(var_end_pos + 2);

            // check length of variable
            if(var_view.length() < 2) {
                log_illegal_variable_type_error(std::string(var_view));
                return std::nullopt;
            }
            auto var_name = std::string(var_view.substr(1));

            // check characters in variable
            auto invalid_char = std::ranges::find_if_not(var_name, [](const char ch) { return std::isalnum(ch) || ch == '_'; });
            if(invalid_char != std::end(var_name)) {
                log_illegal_character_error(*invalid_char);
                return std::nullopt;
            }

            // check variable type
            if(var_view[0] == '@') {
                parts.emplace_back(std::make_unique<UnescapedTemplatePart>(std::move(var_name))); 
            } else if(var_view[0] == '$') {
                parts.emplace_back(std::make_unique<EscapedTemplatePart>(std::move(var_name))); 
            } else {
                log_illegal_variable_type_error(std::string(var_view));
                return std::nullopt;
            }
        } 
    }

    return Template(std::move(parts));
}

auto Template::render(const std::unordered_map<std::string, std::string>& vars) const -> std::optional<std::string> {
    std::string res;

    for(const auto& part : parts) {
        auto s = part->render(vars); 
        if(!s) {
            log_variable_undefined_error(part->get_name());
            return std::nullopt;
        }
        res += s.value();
    }

    return res;
}


auto Template::read_file(std::filesystem::path&& path) -> std::optional<std::string> {
    auto ss = std::ostringstream{};
    std::ifstream input_file(path);
    if (!input_file.is_open()) 
        return std::nullopt;
    ss << input_file.rdbuf();
    return ss.str();
}

void Template::log_unfinished_variable_error() {
    logger::error("Template contains unfinished variable", true);
}

void Template::log_illegal_character_error(char ch) {
    logger::error(fmt::format("Variable contains illegal character: '{}'", ch), true);
}

void Template::log_illegal_variable_type_error(std::string&& name) {
    logger::error(fmt::format("Invalid variable type or variable type not specified in: {}", name), true);
}

void Template::log_variable_undefined_error(std::string&& name) {
    logger::warning(fmt::format("variable required but undefined: {}", name));
}
