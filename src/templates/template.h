#ifndef __TEMPLATES_TEMPLATE_H
#define __TEMPLATES_TEMPLATE_H


#include <filesystem>
#include <memory>
#include <unordered_map>
#include <optional>
#include <vector>
#include <string>


namespace templates {
    class TemplatePart {
    public:
        virtual ~TemplatePart() = default;
        virtual auto render(const std::unordered_map<std::string, std::string>&) const -> std::optional<std::string> = 0;
        virtual auto get_name() const -> std::string { return "[ERROR]"; }
    };

    class StringTemplatePart : public TemplatePart {
    private:
        std::string data;
    public:
        StringTemplatePart(std::string&& data) : data(data) {}
        virtual auto render(const std::unordered_map<std::string, std::string>&) const -> std::optional<std::string> { return data; }
    };

    class UnescapedTemplatePart : public TemplatePart {
    private:
        std::string name;
    public:
        UnescapedTemplatePart(std::string&& name) : name(name) {}
        virtual auto render(const std::unordered_map<std::string, std::string>& vars) const -> std::optional<std::string>;
        virtual auto get_name() const -> std::string { return name; }
    };

    class EscapedTemplatePart : public TemplatePart {
    private:
        std::string name;
    public:
        EscapedTemplatePart(std::string&& name) : name(name) {}
        virtual auto render(const std::unordered_map<std::string, std::string>& vars) const -> std::optional<std::string>;
        virtual auto get_name() const -> std::string { return name; }
    };

    class Template {
    private:
        std::vector<std::unique_ptr<TemplatePart>> parts;        

    public:
        Template() = default;
        Template(std::vector<std::unique_ptr<TemplatePart>>&& parts);

        static auto create(std::filesystem::path&& path) -> std::optional<Template>;

        auto render(const std::unordered_map<std::string, std::string>& vars) const -> std::optional<std::string>;

    private:
        static auto read_file(std::filesystem::path&& path) -> std::optional<std::string>;

        static void log_unfinished_variable_error();
        static void log_illegal_character_error(char ch);
        static void log_illegal_variable_type_error(std::string&& name);
        static void log_variable_undefined_error(std::string&& name);
    };
}


#endif
