#include "template_manager.h"
#include "templates/template.h"

#include <filesystem>
#include <ranges>

#include <fmt/core.h>
#include <fmt/color.h>

#include "util/logger.h"


using namespace templates;


auto TemplateManager::render_template(const std::string& name, const std::unordered_map<std::string, std::string>& variables) const -> 
        std::optional<std::string> {

    auto t = templates.find("templates/" + name + ".tmpl");
    if(t == templates.end()) return std::nullopt;
    return t->second.render(variables);
}

bool TemplateManager::load_templates() {
    std::unordered_map<std::string, Template> new_templates;

    namespace fs = std::filesystem;
    namespace vs = std::views;

    if(!fs::is_directory("templates/")) return true; // no templates -> no error

    auto iter = fs::recursive_directory_iterator("templates/");
    auto tmpl_files = iter |
       vs::filter([](const auto& entry) { return entry.is_regular_file(); }) |
       vs::transform([](const auto file) { return file.path(); }) |
       vs::filter([](const auto& path) { return path.extension() == ".tmpl"; });
    
    for(auto file : tmpl_files) {
        std::string name = file.string();

        auto t = Template::create(std::move(file)); 
        if(!t) {
            log_template_load_error(std::move(name));
            return false;
        }

        new_templates[name] = std::move(t.value());
        log_template_load(std::move(name));
    }

    templates.swap(new_templates);
    return true;
}

void TemplateManager::log_template_load(std::string&& name) {
    logger::info(fmt::format("Loaded template: {}", name), true);
}

void TemplateManager::log_template_load_error(std::string&& name) {
    logger::error(fmt::format("Failed loading template: {}", name), true);
}
