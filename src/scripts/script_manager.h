#ifndef __SCRIPT_MANAGER_H
#define __SCRIPT_MANAGER_H

#include <functional>
#include <string>
#define SOL_ALL_SAFETIES_ON 1
#include <sol/sol.hpp>

#include "path_manager.h"

namespace scripts {
    struct ScriptLoadError: public std::runtime_error { ScriptLoadError() :std::runtime_error("Failed to load script!") {}};

    class ScriptManager {
        sol::state lua;

        PathManager* paths;

    public:
        void load_files(PathManager* paths);

    private:
        void init_libraries();

        void init_http();
        void init_global();
        void init_request();
        void init_response();

        void add_prelude();

        void load_scripts();

        void log_static_register(std::string&& physical_path, std::string&& public_path);
        void log_path_register(std::string&& method, std::string&& path);
        void log_error_register(uint16_t code);

        void log_directory_error();

        void log_file_load(std::string&& path);
        void log_file_load_error(std::string&& path, sol::error&& error);
    };  
};


#endif
