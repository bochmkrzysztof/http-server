#ifndef __GLOBAL_STATE_H
#define __GLOBAL_STATE_H


#include <mutex>
#include <shared_mutex>
#include <string>
#include <unordered_map>
#include <optional>


#include "util/singleton.h"


namespace scripts {
    class GlobalState : public Singleton<GlobalState> {
    private:
        friend class Singleton<GlobalState>;

        mutable std::shared_mutex mutex;
        std::unordered_map<std::string, std::string> values;

        GlobalState() = default;
        ~GlobalState() = default;

    public:
        auto get(const std::string& k) const -> std::optional<std::string>;
        void set(const std::string& k, const std::string& v);
        void rem(const std::string& k);
    };
}


#endif

