#ifndef __PATH_MANAGER_H
#define __PATH_MANAGER_H

#define SOL_ALL_SAFETIES_ON 1
#include <sol/sol.hpp>

#include <unordered_map>
#include <string_view>

#include "http/request.h"
#include "http/response.h"
#include "path_node.h"

#include <fmt/core.h>
#include <filesystem>


namespace fs = std::filesystem;


namespace scripts {
    class PathManager {
        std::unordered_map<uint16_t, sol::function> error_handlers;

        std::optional<std::pair<fs::path, fs::path>> static_path;
        paths::PathNode root;

        std::unordered_map<http::RequestMethod, paths::PathNode> roots;

    public:
        auto insert_path(http::RequestMethod method, std::string_view path, sol::protected_function fn) -> bool;  
        auto set_static_path(std::string_view physical_path, std::string_view public_path) -> bool;
        auto set_error_handler(uint16_t code, sol::protected_function fn) -> bool;

        auto get_path_handler(http::RequestMethod method, std::string_view path) const -> 
            std::optional<std::pair<sol::protected_function, std::unordered_map<std::string, std::string>>>;
        auto get_error_handler(uint16_t code) const -> std::optional<sol::protected_function>;
        auto get_static_path(std::string_view path) const -> std::optional<fs::path>;

        auto has_path_not_using_method(http::RequestMethod method, std::string_view path) const -> bool;
    };
};


#endif
