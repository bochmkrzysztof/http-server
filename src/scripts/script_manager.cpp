#include "script_manager.h"

#include "lua_response.h"
#include "path_manager.h"
#include "global_state.h"

#include "http/request.h"
#include "http/response.h"

#include <fmt/core.h>
#include <fmt/color.h>

#include <filesystem>
#include <ranges>
#include <stdexcept>
#include <string>
#include <string_view>

#include "templates/template_manager.h"
#include "util/html_encode.h"
#include "util/logger.h"


using namespace scripts;




void ScriptManager::load_files(PathManager* paths) {
    this->paths = paths;

    init_libraries();
    load_scripts();

    lua.script("http = nil");

    lua["render_template"] = [](std::string name, const std::unordered_map<std::string, std::string> vars) {
        return templates::TemplateManager::get_instance().render_template(std::move(name), vars); 
    };
}


static int exception_handler(lua_State* L, sol::optional<const std::exception&> maybe_exception, sol::string_view description) {
    (void) maybe_exception;
    // NOP ( preventing default sol2 exception handler from printing error to stdout, it is done later with more usefull information )
    return sol::stack::push(L, description);
}

void ScriptManager::init_libraries() {
    lua.open_libraries(sol::lib::base, sol::lib::package);
    lua.set_exception_handler(exception_handler);

    init_http();
    init_global();
    init_request();
    init_response();

    // misc
    lua["html_entities"] = html_encode;

    // prelude
    add_prelude(); 
}

void ScriptManager::init_http() {
    sol::table http = lua.create_table();

    http["register"] = [&](http::RequestMethod method, std::string path, sol::protected_function callback) {
        std::string method_name = "[UNKNOWN]";

        switch(method) {
            case http::RequestMethod::GET: method_name = "GET"; break;
            case http::RequestMethod::POST: method_name = "POST"; break;
        }

        bool success = paths->insert_path(method, path, callback);
        log_path_register(std::move(method_name), std::move(path));
        if(!success) throw std::runtime_error("Tried to register colliding path!");
    };

    http["register_error"] = [&](uint16_t code, sol::protected_function callback) {
        bool success = paths->set_error_handler(code, callback);
        log_error_register(code);
        if(!success) throw std::runtime_error("Tried to register error handler twice!");   
    };

    http["register_static"] = [&](std::string physical_path, std::string public_path) {
        bool success = paths->set_static_path(physical_path, public_path);
        log_static_register(std::move(physical_path), std::move(public_path));
        if(!success) throw std::runtime_error("Tried to register colliding static path!");
    };

    lua.new_enum("RequestMethod",
        "GET", http::RequestMethod::GET,
        "POST", http::RequestMethod::POST
    ); 

    lua["http"] = http;
}

void ScriptManager::init_global() {
    sol::table global_state = lua.create_table();

    global_state["get"] = [](std::string k) {
        return GlobalState::get_instance().get(k);  
    };

    global_state["set"] = [](std::string k, std::optional<std::string> v) {
        if(v) {
            GlobalState::get_instance().set(k, v.value());  
        } else GlobalState::get_instance().rem(k);
    };

    global_state["rem"] = [](std::string k) {
        GlobalState::get_instance().rem(k);  
    };

    lua["global_state"] = global_state;
}

void ScriptManager::init_request() {
    sol::usertype<http::Request> request = lua.new_usertype<http::Request>("Request");
    request["path_variables"] = sol::readonly_property(&http::Request::get_path_variables);
    request["post"] = sol::readonly_property(&http::Request::get_or_parse_post_params);
    request["cookies"] = sol::readonly_property(&http::Request::get_or_parse_cookies);
    request["headers"] = sol::readonly_property(&http::Request::get_headers);
    request["content"] = sol::readonly_property(&http::Request::get_content);
    request["query"] = sol::readonly_property(&http::Request::get_query);
}

void ScriptManager::init_response() {
    sol::usertype<scripts::LuaResponse> response = lua.new_usertype<scripts::LuaResponse>("Response"); 
    response["set_data"] = &scripts::LuaResponse::set_data;
    response["append_data"] = &scripts::LuaResponse::append_data;
    response["set_status"] = &scripts::LuaResponse::set_status;
    response["add_header"] = &scripts::LuaResponse::add_header;
    response["send_file"] = &scripts::LuaResponse::send_file;
}

void ScriptManager::add_prelude() {
    lua.script(
        // Request methodts
        "GET = RequestMethod.GET\n"
        "POST = RequestMethod.POST\n"

        // Basic response modifiers
        "function Response:html() self:add_header('Content-type', 'text/html') end\n"
        "function Response:push(d) self:append_data(d) end\n"
        "function Response:pushln(d) self:append_data(d .. '\\n') end\n"

        // Redirects
        "function http.register_redirect(dst, src) http.register(GET, src, function(res, req) res:set_status(301)"
        " res:add_header(\"Location\", dst) end) end\n"
        "function Response:redirect(a) self:add_header(\"Location\", a) self:set_status(303) end\n"
        
        // Cookies
        "function Response:set_cookie(n, v) self:add_header(\"Set-Cookie\", n .. \"=\" .. v) end\n"
        "function Response:set_cookie_attr(n, v, m) self:add_header(\"Set-Cookie\", n .. \"=\" .. v .. \"; \" .. m) end\n"
        "function Response:remove_cookie(n) self:set_cookie_attr(n, \"deleted\", \"Max-Age=0\") end\n"

        // Templates
        "function Response:render_template(name, vars) self:set_data(render_template(name, vars)) end\n"
        "function Response:push_template(name, vars) self:append_data(render_template(name, vars)) end\n"
    );
}

void ScriptManager::load_scripts() {
    namespace fs = std::filesystem;
    namespace rs = std::ranges;
    namespace vs = rs::views;

    if(!fs::is_directory("scripts/")) {
        log_directory_error();
        throw ScriptLoadError();
    }

    auto iter = fs::recursive_directory_iterator("scripts/");
    auto lua_files = iter |
       vs::filter([](const auto& entry) { return entry.is_regular_file(); }) |
       vs::transform([](const auto file) { return file.path(); }) |
       vs::filter([](const auto& path) { return path.extension() == ".lua"; });

    bool ok = true;
    for(const auto& file : lua_files) {
        lua.safe_script_file(file, [&](lua_State*, sol::protected_function_result pfr) {
            sol::error err = pfr;
            log_file_load_error(std::move(file.string()), std::move(err));

            ok = false;
            return pfr;
        });

        if(ok) log_file_load(std::move(file.string()));
        if(!ok) throw ScriptLoadError(); 
    }
}

void ScriptManager::log_static_register(std::string&& physical_path, std::string&& public_path) {
    logger::debug(fmt::format("Registered static path {} → {}", public_path, physical_path), true);
}

void ScriptManager::log_path_register(std::string&& method, std::string&& path) {
    logger::debug(fmt::format("Registered callback {} → {}", method, path), true);
}

void ScriptManager::log_error_register(uint16_t code) {
    logger::debug(fmt::format("Registered error handler {}", code), true);
}

void ScriptManager::log_directory_error() {
    logger::error("Scripts directory does not exist", true);
}

void ScriptManager::log_file_load(std::string&& path) {
    logger::info(fmt::format("Loaded lua file: {}", path), true);
}

void ScriptManager::log_file_load_error(std::string&& path, sol::error&& error) {
    logger::error(fmt::format("Error while loading lua file: {}\n{}", path, error.what()), true);
}
