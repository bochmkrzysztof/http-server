#include "global_state.h"

#include <mutex>
#include <optional>
#include <string>


using namespace scripts;


auto GlobalState::get(const std::string& k) const -> std::optional<std::string> {
    std::shared_lock l(mutex);
    
    auto el = values.find(k);
    if(el != values.end()) {
        return el->second;        
    } return std::nullopt;
}

void GlobalState::set(const std::string& k, const std::string& v) {
    std::unique_lock l(mutex);
    values[k] = v;
}

void GlobalState::rem(const std::string& k) {
    std::unique_lock l(mutex);

    auto el = values.find(k);
    if(el != values.end())
        values.erase(el);
}
