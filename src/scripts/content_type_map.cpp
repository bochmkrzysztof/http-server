#include "content_type_map.h"


using namespace scripts;


ContentTypeMap::ContentTypeMap() {
    map.emplace(std::make_pair(".js", "application/javascript"));
    map.emplace(std::make_pair(".ogg", "application/ogg"));
    map.emplace(std::make_pair(".pdf", "application/pdf"));
    map.emplace(std::make_pair(".json", "application/json"));
    map.emplace(std::make_pair(".xml", "application/xml"));
    map.emplace(std::make_pair(".zip", "application/zip"));

    map.emplace(std::make_pair(".gif", "image/gif"));
    map.emplace(std::make_pair(".jpg", "image/jpeg"));
    map.emplace(std::make_pair(".png", "image/png"));
    map.emplace(std::make_pair(".tiff", "image/tiff"));

    map.emplace(std::make_pair(".css", "text/css"));
    map.emplace(std::make_pair(".csv", "text/csv"));
    map.emplace(std::make_pair(".html", "text/html"));
    map.emplace(std::make_pair(".htm", "text/html"));
    map.emplace(std::make_pair(".txt", "text/plain"));
}

auto ContentTypeMap::get_content_type(std::string_view file_extention) const -> std::optional<std::string_view> {
    auto result = map.find(file_extention);
    if(result != map.end()) {
        return result->second;
    } else return std::nullopt;
}
