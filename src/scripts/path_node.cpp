#include "path_node.h"
#include "http/escape.h"

#include <memory>
#include <optional>
#include <regex>
#include <sol/forward.hpp>
#include <string>
#include <string_view>
#include <tuple>
#include <utility>
#include <variant>
#include <cassert>
#include <fmt/ranges.h>


using namespace paths;


PathNode::PathNode(sol::protected_function fn) :root(fn) {}


// Basic setters / getters

auto PathNode::get_node(std::string_view path) const -> std::optional<std::pair<std::optional<const std::string*>, const PathNode*>> {
    auto decoded_path = http::escape::decode(path);

    if(basic_children) {
        auto& map = basic_children.value(); 
        auto result = map->find(decoded_path);

        if(result != map->end())
            return std::make_pair(std::nullopt, &result->second); 
    }

    if(regex_children) {
        auto& vec = regex_children.value();
        for(auto& [r, n, p] : vec) {
            if(r) {
                std::regex regex = r.value(); 
                std::smatch res;
                if(std::regex_match(decoded_path, res, regex))
                    return std::make_pair(&n, &p);
            } else return std::make_pair(&n, &p);
        } 
    }

    return std::nullopt;
}

auto PathNode::set_root(sol::protected_function fn) -> bool {
    if(root) return false;

    root = fn; 
    return true;
}

auto PathNode::get_root() const -> std::optional<sol::protected_function> {
    return root;
}





// Node insertion functions

auto PathNode::insert_node(const std::string& path) -> PathNode& {
    if(!basic_children)
        basic_children = std::make_unique<std::unordered_map<std::string, PathNode>>();

    auto& map = basic_children.value();
    return map->emplace(path, PathNode()).first->second;
}

auto PathNode::get_or_insert_node(std::string_view path) -> PathNode& {
    if(path.starts_with('$')) {
        return get_or_insert_regex_node(std::string(path));
    } else {
        auto decoded_path = http::escape::decode(path);

        if(basic_children) {
            auto& map = basic_children.value();
            auto result = map->find(decoded_path);

            if(result != map->end()) 
                return result->second; 
        } 

        return insert_node(decoded_path);
    }
}



// regex node creation

auto PathNode::insert_regex_node(const std::string& name, const std::optional<std::string>& regex_src) -> PathNode& {
    if(!regex_children)
        regex_children = RegexChildrenVector();

    std::optional<std::regex> regex = regex_src.has_value() ? std::optional(std::regex(regex_src.value())) : std::nullopt;

    auto& vec = regex_children.value();
    vec.push_back(std::make_tuple(regex, name, PathNode()));
    return std::get<2>(vec.back());
}

auto PathNode::get_or_insert_regex_node(const std::string& path) -> PathNode& {
    std::string_view path_view = path; 
    path_view.remove_prefix(1);

    std::optional<std::string> var_regex;

    auto colon_pos = path_view.find_first_of(';');
    auto var_name = path_view.substr(0, colon_pos);

    if(colon_pos != std::string_view::npos) {
        auto regex = path_view;
        regex.remove_prefix(colon_pos + 1);

        if(regex.size() > 0)
            var_regex = regex;
    }

    if(regex_children) 
        for(auto& [r, n, p] : regex_children.value())
            if(n == var_name) return p; 

    return insert_regex_node(std::string(var_name), var_regex);
}




// Full path functions 

auto PathNode::insert_path(const std::string_view path, sol::protected_function fn) -> bool {
    if(path.size() == 0) {
        return set_root(fn);
    } else {
        auto pos = path.find_first_of('/'); 

        if(pos == std::string_view::npos) {
            auto& node = get_or_insert_node(path);
            return node.set_root(fn);        
        } else {
            auto suffix = path;
            suffix.remove_prefix(pos + 1);
            
            if(pos == 0)
                return insert_path(suffix, fn); 
            else {
                auto part = path.substr(0, pos);
                auto& node = get_or_insert_node(part);
                return node.insert_path(suffix, fn);
            }
        }
    }
}

auto PathNode::get_path(const std::string_view path, std::unordered_map<std::string, std::string>& path_variables) const -> 
            std::optional<sol::protected_function> {

    if(path.size() == 0)
        return root;

    auto pos = path.find_first_of('/');

    if(pos == std::string_view::npos) {
        auto node = get_node(path);
        if(node) {
            auto& [name, node_val] = node.value(); 
            if(name) path_variables[**name] = path;
            return node_val->get_root(); 
        } else return std::nullopt;
    } else {
        auto suffix = path;
        suffix.remove_prefix(pos + 1);

        if(pos == 0) return get_path(suffix, path_variables);
    
        auto part = path.substr(0, pos);
        auto node = get_node(part);

        if(node) {
            auto& [name, node_val] = node.value(); 
            if(name) path_variables[**name] = part;
            return node_val->get_path(suffix, path_variables); 
        } else return std::nullopt;
    }
}

