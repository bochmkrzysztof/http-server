#ifndef __SCRIPTS_RESPONSE_MANAGER_H
#define __SCRIPTS_RESPONSE_MANAGER_H


#include "http/request.h"
#include "http/response.h"
#include "path_manager.h"


namespace scripts {
    class ResponseManager {
    private:
        const PathManager& path_manager;

    public:
        ResponseManager(const PathManager& path_manager);

        auto handle_request_and_headers(http::Request& request) const -> http::Response;
        auto handle_error_and_headers(uint16_t) const -> http::Response;

        auto handle_request(http::Request& request) const -> http::Response;
        auto handle_error(uint16_t code) const -> http::Response;

    private:
        void add_default_headers(http::Response& resp) const;

        auto get_default_error_message(uint16_t code) const -> http::Response;

        auto handle_static_path(const http::Request& request) const -> std::optional<http::Response>;
        auto handle_normal_path(http::Request& request) const -> std::optional<http::Response>;

        auto handle_normal_file(const fs::path& path) const -> std::optional<http::Response>;
        auto handle_directory(const fs::path& path) const -> std::optional<http::Response>;

        void log_lua_error(sol::error&& error) const;
    };
}


#endif
