#include "path_manager.h"
#include "lua_response.h"

#include "http/request.h"
#include "http/response.h"

#include <filesystem>
#include <iterator>
#include <ranges>
#include <string>
#include <string_view>

#include <fmt/color.h>
#include <fmt/core.h>
#include <unordered_map>
#include <utility>
#include <vector>

#include "scripts/path_node.h"
#include "util/logger.h"


using namespace scripts;



auto PathManager::insert_path(http::RequestMethod method, std::string_view path, sol::protected_function fn) -> bool {
    return roots[method].insert_path(path, fn); 
}

auto PathManager::set_error_handler(uint16_t code, sol::protected_function fn) -> bool {
    if(error_handlers.contains(code)) return false;
    error_handlers[code] = fn;
    return true;
}

auto PathManager::set_static_path(std::string_view physical_path, std::string_view public_path) -> bool {
    if(static_path) return false;

    auto pub = fs::path(public_path).relative_path();
    if(pub.empty()) return false;

    auto phys = fs::path(physical_path).relative_path();
    if(phys.empty()) return false;

    static_path = std::make_pair(pub, phys);

    return true;
}

auto PathManager::get_path_handler(http::RequestMethod method, std::string_view path) const ->
        std::optional<std::pair<sol::protected_function, std::unordered_map<std::string, std::string>>> {
   
    std::unordered_map<std::string, std::string> path_variables; 

    auto root_iter = roots.find(method);
    if(root_iter != roots.end()) {
        auto handler = root_iter->second.get_path(path, path_variables);
        if(handler) return std::make_pair(handler.value(), std::move(path_variables));
    }

    return std::nullopt;
}

auto PathManager::get_error_handler(uint16_t code) const -> std::optional<sol::protected_function> {
    auto handler = error_handlers.find(code);
    if(handler != error_handlers.end()) 
        return handler->second;
    return std::nullopt;
}

auto PathManager::get_static_path(std::string_view path) const -> std::optional<fs::path> {
    if(static_path) {
        const auto& [pub, phys] = static_path.value();
        auto file_path = fs::path(path).relative_path().lexically_relative(pub);

        if(std::ranges::none_of(file_path, [](const std::string& str) { return str == ".."; }))
            return phys / file_path; 
    } 
    
    return std::nullopt;
}

auto PathManager::has_path_not_using_method(http::RequestMethod method, std::string_view path) const -> bool {
    for(const auto& [m, root] : roots) {
        if(m != method) {
            std::unordered_map<std::string, std::string> map;
            if(root.get_path(path, map)) 
                return true;
        }   
    }

    return false;
}
