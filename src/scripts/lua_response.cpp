#include "lua_response.h"
#include "http/response.h"

#include <stdexcept>


using namespace scripts;


LuaResponse::LuaResponse(http::Response& response)
    :response(response) {}

void LuaResponse::append_data(std::string d) {
    if(!response.append_data(std::move(d)))
        throw std::runtime_error("Failed to append data, probably already sending file!");
}

void LuaResponse::set_data(std::string d) {
    if(!response.set_data(std::move(d)))
        throw std::runtime_error("Failed to set data, probably already has data or sending file!");
}

void LuaResponse::send_file(std::string filen) {
    if(!response.send_file(std::move(filen)))
        throw std::runtime_error("Failed to send file, probably already has data or sending file");
}

void LuaResponse::add_header(std::string k, std::string v) {
    response.add_header(std::move(k), std::move(v));
}

void LuaResponse::set_status(uint16_t code) {
    response.set_status(code);
}
