#ifndef __PATH_NODE_H
#define __PATH_NODE_H

#define SOL_ALL_SAFETIES_ON 1
#include <sol/sol.hpp>
#include <sol/forward.hpp>


#include <string_view>
#include <unordered_map>
#include <memory>
#include <variant>
#include <string>
#include <optional>
#include <vector>
#include <regex>

#include <fmt/format.h>


namespace paths {
    class PathNode {
    private:
        typedef std::unique_ptr<std::unordered_map<std::string, PathNode>> BasicChildrenMap;
        typedef std::vector<std::tuple<std::optional<std::regex>, std::string, PathNode>> RegexChildrenVector;

        std::optional<sol::protected_function> root;

        std::optional<BasicChildrenMap> basic_children;
        std::optional<RegexChildrenVector> regex_children;

    public:
        PathNode() = default;                       //< Creates empty node
        PathNode(PathNode&&) = default;             //< Moves node

        PathNode(sol::protected_function root_fn);  //< Creates node with it's root element

        auto insert_path(const std::string_view path, sol::protected_function fn) -> bool;
        auto get_path(const std::string_view path, std::unordered_map<std::string, std::string>& path_variables) const -> 
            std::optional<sol::protected_function>;

    private:
        auto get_node(std::string_view path) const -> 
            std::optional<std::pair<std::optional<const std::string*>, const PathNode*>>;

        auto get_root() const -> std::optional<sol::protected_function>;
        auto set_root(sol::protected_function fn) -> bool;

        auto insert_node(const std::string& path) -> PathNode&;
        auto get_or_insert_node(std::string_view path) -> PathNode&;

        auto insert_regex_node(const std::string& name, const std::optional<std::string>& regex) -> PathNode&;
        auto get_or_insert_regex_node(const std::string& path) -> PathNode&;

        PathNode(const PathNode&) = delete;
        PathNode& operator=(const PathNode&) = delete;
    };
};


#endif
