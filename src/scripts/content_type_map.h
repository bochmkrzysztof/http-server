#ifndef __SCRIPTS_CONTENT_TYPE_MAP_H
#define __SCRIPTS_CONTENT_TYPE_MAP_H


#include "util/singleton.h"

#include <unordered_map>
#include <string_view>
#include <optional>


namespace scripts {
    class ContentTypeMap : public Singleton<const ContentTypeMap> {
    private:
        friend class Singleton<const ContentTypeMap>;

        std::unordered_map<std::string_view, std::string_view> map;

        ContentTypeMap();
        ~ContentTypeMap() = default;

    public:
        auto get_content_type(std::string_view file_extention) const -> std::optional<std::string_view>;
    };
}


#endif
