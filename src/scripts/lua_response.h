#ifndef __LUA_RESPONSE_H
#define __LUA_RESPONSE_H


#include "http/response.h"
#include <string>


namespace scripts {
    // http::Response adaptor that throws exceptions on error
    // unlike http::Responce which returns false

    class LuaResponse {
    private:
        http::Response& response;
    
    public:
        LuaResponse(http::Response& response);

        void append_data(std::string d);
        void set_data(std::string d);

        void send_file(std::string filen);

        void set_status(uint16_t code);
        void add_header(std::string k, std::string v);
    };
}


#endif
