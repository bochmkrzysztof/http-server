#include "response_manager.h"
#include "http/request.h"
#include "http/response.h"
#include "scripts/content_type_map.h"
#include "scripts/lua_response.h"
#include "util/logger.h"
#include <filesystem>


using namespace scripts;



ResponseManager::ResponseManager(const PathManager& path_manager)
    :path_manager(path_manager) {}

auto ResponseManager::handle_request_and_headers(http::Request& request) const -> http::Response {
    http::Response res = handle_request(request);
    add_default_headers(res);
    return res;
}

auto ResponseManager::handle_error_and_headers(uint16_t code) const -> http::Response {
    http::Response res = handle_error(code);
    add_default_headers(res);
    return res;
}

auto ResponseManager::handle_request(http::Request& request) const -> http::Response {
    std::optional<http::Response> res = handle_static_path(request);
    if(!res) res = handle_normal_path(request);

    http::Response resp = res ? std::move(res.value()) : handle_error(404);
    return resp;
}

auto ResponseManager::handle_error(uint16_t code) const -> http::Response {
    auto handler = path_manager.get_error_handler(code); 

    if(handler) {
        auto res = http::Response(http::ResponseStatus(code));
        scripts::LuaResponse lua_res(res);

        sol::protected_function_result result = handler.value()(lua_res);

        if(!result.valid()) {
            sol::error err = result; 
            log_lua_error(std::move(err));

            if(code != 500) {
                return handle_error(500);
            } else return get_default_error_message(code);
        } else return res;
    } else return get_default_error_message(code);
}

void ResponseManager::add_default_headers(http::Response& res) const {
    if(!res.get_headers().contains("Server")) 
        res.add_header("Server", "KB server v1.1.0");
    res.calculate_length();
}

auto ResponseManager::get_default_error_message(uint16_t code) const -> http::Response {
    auto status = http::ResponseStatus(code);

    auto resp = http::Response(status, 
            fmt::format("<http><head><title>Error {}</title></head><body><h1>Error {} - {}</h1></body></http>", 
                status.get_code(), 
                status.get_code(), 
                status.get_name()));

    resp.add_header("Content-type", "text/html");
    return resp;
}

auto ResponseManager::handle_static_path(const http::Request& request) const -> std::optional<http::Response> {
    auto result = path_manager.get_static_path(request.get_path());

    if(result) {
        auto path = std::move(result.value());         

        auto res = handle_normal_file(path); 
        if(!res) res = handle_directory(path);

        return res;
    } else return std::nullopt;
}

auto ResponseManager::handle_normal_path(http::Request& request) const -> std::optional<http::Response> {
    auto result = path_manager.get_path_handler(request.get_method(), request.get_path()); 
    
    if(result) {
        auto [handler, path_variables] = std::move(result.value());
        request.set_path_variables(std::move(path_variables));

        auto res = http::Response(http::ResponseStatus(200)); 
        scripts::LuaResponse lua_res(res);

        sol::protected_function_result result = handler(lua_res, request);

        if(!result.valid()) {
            sol::error err = result;
            log_lua_error(std::move(err));
            return handle_error(500);
        } else return res;
    } else if(path_manager.has_path_not_using_method(request.get_method(), request.get_path())) {
        return handle_error(405);
    } else return std::nullopt;
}

auto ResponseManager::handle_normal_file(const fs::path& path) const -> std::optional<http::Response> {
    if(fs::is_regular_file(path)) {
        auto res = http::Response(http::ResponseStatus(200));
        res.add_header("Cache-Control", "max-age=31536000");

        auto type = ContentTypeMap::get_instance().get_content_type(path.extension().string());

        if(type)
            res.add_header("Content-type", std::string(type.value()));
        res.send_file(path.string());

        return res;
    } else return std::nullopt;
}

auto ResponseManager::handle_directory(const fs::path& path) const -> std::optional<http::Response> {
    if(fs::is_directory(path)) {
        auto res = http::Response(http::ResponseStatus(200));
        res.set_data("This is directory, but directory listing is not yet implemented!");
        return res;
    } else return std::nullopt;
}

void ResponseManager::log_lua_error(sol::error&& error) const {
    logger::warning(fmt::format("Lua request handler execution failtd:\n{}", error.what()));
}
