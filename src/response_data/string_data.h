#ifndef __STRING_DATA_H
#define __STRING_DATA_H


#include "response_data.h"

#include <optional>
#include <string_view>


namespace data {
    class StringData :public http::ResponseData {
    private:
        std::string data;
        std::optional<std::string_view> data_view;

        StringData(const StringData&) = delete;
        auto operator=(const StringData&) -> StringData& = delete;
    
    public:
        StringData() = default;
        StringData(std::string data);

        virtual ~StringData() = default;

        virtual void get_data(io::streambuf& buffer, std::function<void(boost::system::error_code, size_t size)> callback);

        virtual auto append_data(std::string d) -> bool;
        virtual auto get_length() const -> size_t;
    };
}


#endif
