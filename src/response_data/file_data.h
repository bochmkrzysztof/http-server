#ifndef __FILE_DATA_H
#define __FILE_DATA_H

#include "response_data.h"
#include <boost/asio.hpp>


namespace data {
    class FileData: public http::ResponseData {
    private:    
        io::stream_file file;

        size_t size;

    public:
        FileData(std::string filen);
        virtual ~FileData() = default;

        virtual void get_data(io::streambuf& buffer, std::function<void(boost::system::error_code, size_t size)> callback);
        virtual auto get_length() const -> size_t { return size; }
    };
}


#endif
