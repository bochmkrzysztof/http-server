#ifndef __HTTP_REQUEST_DATA_H
#define __HTTP_REQUEST_DATA_H

#include <functional>

#include <boost/asio.hpp>

namespace io = boost::asio;

namespace http {
    class ResponseData {
    public:
        virtual ~ResponseData() = default;

        virtual void get_data(io::streambuf& buffer, std::function<void(boost::system::error_code, size_t size)> callback) = 0;

        virtual auto append_data(std::string d) -> bool;
        virtual auto get_length() const -> size_t;
    };
}

#endif
