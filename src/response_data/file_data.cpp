#include "file_data.h"
#include "basic_serwer/asio_context.h"

#include <filesystem>

using namespace data;


FileData::FileData(std::string filen) :file(AsioContext::get_instance().get_ctx()) {
    size = std::filesystem::file_size(filen);

    boost::system::error_code error;
    file.open(std::move(filen), io::file_base::read_only, error);
}

void FileData::get_data(io::streambuf& buffer, std::function<void(boost::system::error_code, size_t size)> callback) {
    io::async_read(file, buffer, callback);
}
