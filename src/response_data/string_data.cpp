#include "string_data.h"
#include <cassert>
#include <string_view>

#include <fmt/core.h>

using namespace data;

StringData::StringData(std::string d) :data(std::move(d)) {} 

void StringData::get_data(io::streambuf& buffer, std::function<void(boost::system::error_code, size_t size)> callback) {
    std::string_view view = data_view.value_or(data);

    size_t data_size = view.size();
    size_t buffer_size = buffer.max_size();
    size_t sent_data_size = std::min(data_size, buffer_size);

    std::string_view prefix = view.substr(0, sent_data_size);
    view.remove_prefix(prefix.length());

    data_view = view;

    std::ostream os(&buffer);
    os << prefix;    

    boost::system::error_code error;

    if(view.empty())
        error = io::error::eof;

    callback(error, sent_data_size);
}

auto StringData::append_data(std::string d) -> bool {
    assert(!data_view);

    data += d;
    return true;
}

auto StringData::get_length() const -> size_t {
    return data.length();
}

