#include "response_data.h"

#include <fmt/core.h>
#include <fmt/color.h>

#include "util/logger.h"


using namespace http;



auto ResponseData::append_data(std::string) -> bool {
    logger::warning("Trying to append data to data type that does not support appending!");
    return false;
}

auto ResponseData::get_length() const -> size_t {
    return 0;
}
