#include "basic_serwer/basic_async_server.h"
#include "http/server.h"

#include "scripts/script_manager.h"
#include "scripts/path_manager.h"

#include "templates/template_manager.h"

#include "basic_serwer/asio_context.h"
#include "basic_serwer/serwer_thread.h"

#include <boost/system/system_error.hpp>
#include <boost/throw_exception.hpp>

#include <cstdio>
#include <memory>
#include <optional>
#include <thread>
#include <chrono>
#include <latch>
#include <atomic>

#include <fmt/color.h>
#include <fmt/core.h>

#include "util/logger.h"



#define N_THREADS 4
#define PORT 8080


#define EXIT_NORMAL         0
#define EXIT_INIT_ERROR     1
#define EXIT_BIND_ERROR     2
#define EXIT_COMMAND_LINE   3


void setup_log_level() {
    const char* level = getenv("LOG_LEVEL");

    if(!level) {
        logger::Logger::get_instance().set_log_level(logger::INFO);    
    } else if(strcmp(level, "TRACE") == 0) {
        logger::Logger::get_instance().set_log_level(logger::TRACE);    
    } else if(strcmp(level, "DEBUG") == 0) {
        logger::Logger::get_instance().set_log_level(logger::DEBUG);    
    } else if(strcmp(level, "INFO") == 0) {
        logger::Logger::get_instance().set_log_level(logger::INFO);    
    } else if(strcmp(level, "WARNING") == 0) {
        logger::Logger::get_instance().set_log_level(logger::WARNING);    
    } else if(strcmp(level, "ERROR") == 0) {
        logger::Logger::get_instance().set_log_level(logger::ERROR);    
    } else {
        logger::Logger::get_instance().set_log_level(logger::INFO);    
    }
}

void print_usage(const char* program_name) {
    fmt::print("Usage:\n");
    fmt::print("\t{} [OPTIONS]\n", program_name);
    fmt::print("\n");
    fmt::print("Options:\n");
    fmt::print("\t-h - print this message\n");
    fmt::print("\t-p <PORT> - specify port number on which serwer should listen\n");
    fmt::print("\t-t <THREADS> - specify number of worker threads\n");
}

auto atoull(const char* str) -> size_t {
    ssize_t i = atoll(str); 
    return i > 0 ? i : 0;
}

auto get_command_line(const char** argv) -> std::optional<std::pair<size_t, uint16_t>> {
    size_t n_threads = N_THREADS;
    uint16_t port = PORT; 

    if(const char* program_name = *argv++) {
        while(const char* arg = *argv++) {
            if(strcmp(arg, "-t") == 0) {
                if(const char* t = *argv++) {
                    n_threads = atoull(t);
                    if(n_threads == 0) {
                        fmt::print("Invalid usage of '-t' argument,\n");
                        fmt::print("number of worker threads must be positive\n\n");
                        print_usage(program_name);
                        return std::nullopt;
                    }
                } else {
                    fmt::print("Invalid usage of '-t' argument,\n");
                    fmt::print("please specify the number of worker threads\n\n");
                    print_usage(program_name);
                    return std::nullopt;
                }
            } else if(strcmp(arg, "-p") == 0) {
                if(const char* t = *argv++) {
                    port = atoull(t);
                } else {
                    fmt::print("Invalid usage of '-p' argument,\n");
                    fmt::print("please specify the port on which server should listen\n\n");
                    print_usage(program_name);
                    return std::nullopt;
                }
            } else if(strcmp(arg, "-h") == 0) {
                print_usage(program_name);
                return std::nullopt;
            } else {
                fmt::print("Invalid argument: '{}'\n", arg);
                print_usage(program_name);
                return std::nullopt;
            } 
        }     
    }

    return std::make_pair(n_threads, port); 
}



void log_acceptor_create_error(boost::system::system_error&& error) {
    logger::error(fmt::format("Failed to create acceptor: {}", error.what()));
}

auto create_worker() -> bool {
    try { // just create nothing more to do
        threads::WorkerThread::get_thread_local_instance();
    } catch(scripts::ScriptLoadError&) {
            return false;
    }

    return true;
}

auto create_threads(size_t n_threads) -> std::optional<std::vector<std::jthread>> {
    std::vector<std::jthread> workers;

    auto loading_finished = std::make_shared<std::latch>(n_threads + 1); 

    auto loaded_successfully = std::make_shared<std::atomic_bool>(true);

    // init worker threads
    for(size_t i = 0; i < n_threads; i++) {
        workers.emplace_back([loaded_successfully, loading_finished](){
            auto latch = std::move(loading_finished);

            if(!create_worker())
                *loaded_successfully = false;

            logger::flush();

            latch->arrive_and_wait();
            latch.reset();

            if(*loaded_successfully)
                AsioContext::get_instance().get_ctx().run();
        });
    }

    // load templates
    std::jthread template_loader([loaded_successfully, loading_finished]() {
        auto latch = std::move(loading_finished);
        
        if(!templates::TemplateManager::get_instance().load_templates())
            *loaded_successfully = false;        

        logger::flush();
        latch->count_down();
    });     

    loading_finished->wait();

    if(*loaded_successfully)
        return workers;
    else return std::nullopt;
}

auto create_logger() -> std::jthread {
    std::jthread log([]{ 
        logger::Logger::get_instance().run(); 
    }); 

    return log;
}

auto create_server(uint16_t port) -> std::optional<http::Server> {
    std::optional<http::Server> srv;

    try {
        srv.emplace(port);
    } catch (boost::wrapexcept<boost::system::system_error>& error) {
        log_acceptor_create_error(std::move(error));
    } 

    return srv;
}

int main(int, const char** argv) {
    setup_log_level();

    using work_guard_type = boost::asio::executor_work_guard<boost::asio::io_context::executor_type>;

    auto args = get_command_line(argv);
    if(!args) return EXIT_COMMAND_LINE;

    auto [n_threads, port] = args.value();

    auto& ctx = AsioContext::get_instance().get_ctx();
    auto work_guard = std::make_unique<work_guard_type>(ctx.get_executor());

    auto log = create_logger();
    auto workers = create_threads(n_threads);

    if(!workers) {
        logger::Logger::get_instance().finish();
        return EXIT_INIT_ERROR;
    }

    auto srv = create_server(port);
    work_guard.reset();

    if(srv) {
        while(getchar() != EOF);
        ctx.stop(); 
        
        logger::Logger::get_instance().finish();
        return EXIT_NORMAL; 
    } else {
        logger::Logger::get_instance().finish();
        return EXIT_BIND_ERROR;
    }
}
