#include "logger.h"
#include <chrono>

#include <fmt/chrono.h>
#include <fmt/color.h>

#include <mutex>

#include <fmt/core.h>
#include <sstream>
#include <string_view>
#include <tuple>


using namespace logger;



static auto get_log_color(Severity severity) {
    switch (severity) {
        case Severity::TRACE: return fmt::color::aqua;
        case Severity::DEBUG: return fmt::color::yellow_green;
        case Severity::INFO: return fmt::color::blue;
        case Severity::WARNING: return fmt::color::orange;
        case Severity::ERROR: return fmt::color::crimson;
    }
    return fmt::color::white;
}

static auto get_log_severity(Severity severity) -> std::string_view {
    switch (severity) {
        case Severity::TRACE: return "TRACE";
        case Severity::DEBUG: return "DEBUG";
        case Severity::INFO: return "INFO";
        case Severity::WARNING: return "WARNING";
        case Severity::ERROR: return "ERROR";
    }
    return "UNKNOWN";
}

void Logger::log(std::thread::id thread_id, std::string&& msg, Severity severity, bool init) {
    if(severity >= log_level) 
        queue.push(std::make_pair(thread_id, std::make_tuple(std::move(msg), severity, init)));
} 

void Logger::flush_init(std::thread::id id) {
    queue.push(std::make_pair(id, std::nullopt));
}

void Logger::run() {
    while(1) {
        auto job = queue.pop();
        if(!job) return;

        auto [id, msg] = std::move(job.value());
        if(msg) {
            auto [str, severity, init] = std::move(msg.value());

            if(init) {
                auto& [msgs, worst_severity] = cached_init_messages[id];
                if(severity > worst_severity) worst_severity = severity;
                msgs.push_back(std::make_pair(std::move(str), severity)); 
            } else {
                fmt::print("{} [{}] {}\n", std::chrono::high_resolution_clock::now(), fmt::styled(get_log_severity(severity), fg(get_log_color(severity))), str); 
            } 
        } else { // flush init
            auto [msgs, severity] = std::move(cached_init_messages[id]);
            auto color = get_log_color(severity);

            if(msgs.empty()) return;

            std::stringstream str; str << std::hex << id;

            fmt::print(fg(color) | fmt::emphasis::bold, 
                    "===============================================\n");
            fmt::print(fg(color) | fmt::emphasis::bold, 
                    "Init messages from thread: {}\n", str.view());

            for(const auto& [str, severity] : msgs)
                fmt::print("{} [{}] {}\n", std::chrono::high_resolution_clock::now(), fmt::styled(get_log_severity(severity), fg(get_log_color(severity))), str); 

            fmt::print(fg(color) | fmt::emphasis::bold, 
                    "===============================================\n\n");
        }
    }
}

void Logger::finish() {
    queue.finish();
}

void logger::flush() {
    auto id = std::this_thread::get_id(); 
    Logger::get_instance().flush_init(id);
}

void logger::log(std::string&& msg, Severity severity, bool init) {
    auto id = std::this_thread::get_id(); 
    Logger::get_instance().log(id, std::move(msg), severity, init);
}

void logger::trace(std::string&& msg, bool init) {
    logger::log(std::move(msg), Severity::TRACE, init);
}

void logger::debug(std::string&& msg, bool init) {
    logger::log(std::move(msg), Severity::DEBUG, init);
}

void logger::info(std::string&& msg, bool init) {
    logger::log(std::move(msg), Severity::INFO, init);
}

void logger::warning(std::string&& msg, bool init) {
    logger::log(std::move(msg), Severity::WARNING, init);
}

void logger::error(std::string&& msg, bool init) {
    logger::log(std::move(msg), Severity::ERROR, init);
}


