#include "html_encode.h"


auto html_encode(std::string_view string) -> std::string {
    std::string res;

    for(char ch : string) {
        switch(ch) {
            case '<': res += "&lt;"; break;
            case '>': res += "&gt;"; break;
            case '"': res += "&quot;"; break;
            case '\'': res += "&apos;"; break;
            case '&': res += "&amp;"; break;
            default: res += ch;
        } 
    }

    return res; 
}

