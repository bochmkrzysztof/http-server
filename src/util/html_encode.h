#ifndef __UTIL_HTML_ENCODE_H
#define __UTIL_HTML_ENCODE_H


#include <string>
#include <string_view>


auto html_encode(std::string_view string) -> std::string;


#endif
