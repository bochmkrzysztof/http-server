#ifndef __SYNCHRONIZED_QUEUE_H
#define __SYNCHRONIZED_QUEUE_H


#include <queue>
#include <mutex>
#include <semaphore>
#include <optional>


namespace threads {
    template<typename T>
    class SynchronizedQueue {
        std::queue<T> queue; 
        std::mutex mutex;
        std::counting_semaphore<0> semaphore;

    public:
        SynchronizedQueue() :semaphore(0) {}

        auto pop() -> std::optional<T> {
            semaphore.acquire(); 

            if(queue.size()) {
                std::lock_guard l(mutex);
                T s = std::move(queue.front());
                queue.pop();
                return s;
            } else return std::nullopt;
        }

        void push(T&& s) {
            {
                std::lock_guard l(mutex);
                queue.push(std::move(s));
            }

            semaphore.release();
        }

        void finish(size_t n = 1) {
            semaphore.release(n); 
        }
    };

}


#endif
