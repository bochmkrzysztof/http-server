#ifndef __SINGLETON_H
#define __SINGLETON_H


#include <type_traits>


template<typename T>
class Singleton {
private:
    Singleton(Singleton&&) = delete;
    Singleton(const Singleton&) = delete;

    auto operator=(Singleton&&) = delete;
    auto operator=(const Singleton&) = delete;

protected:
    Singleton() noexcept = default;

public:
    static T& get_instance() noexcept(std::is_nothrow_constructible<T>::value) {
        static T instance; 
        return instance;
    }

    static T& get_thread_local_instance() noexcept(std::is_nothrow_constructible<T>::value) {
        static thread_local T instance;
        return instance;
    } 
};


#endif
