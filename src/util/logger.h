#ifndef __LOGGER_H
#define __LOGGER_H

#include <functional>
#include <memory>
#include <mutex>
#include <thread>
#include <unordered_map>
#include <unordered_set>

#include "synchronized_queue.h"
#include "singleton.h"


namespace logger {
    enum Severity {
        TRACE, DEBUG, INFO, WARNING, ERROR
    };

    class LogMessage {
        std::function<void()> msg;

        bool init;
        bool error;

    public:
        LogMessage(std::function<void()> msg, bool init = false, bool error = false) :msg(msg), init(init), error(error) {}
        virtual ~LogMessage() {}; 

        virtual bool is_init() { return init; }
        virtual bool is_error() { return error; }

        void log() { msg(); };
    };

    class Logger : public Singleton<Logger> {
        friend class Singleton<Logger>;

        Severity log_level = Severity::TRACE;

        threads::SynchronizedQueue<std::pair<std::thread::id, std::optional<std::tuple<std::string, Severity, bool>>>> queue;
        std::unordered_map<std::thread::id, std::pair<std::vector<std::pair<std::string, Severity>>, Severity>> cached_init_messages;

        Logger() = default;
        ~Logger() = default;

    public:
        void log(std::thread::id thread_id, std::string&& msg, Severity severity = Severity::INFO, bool init = false); 
        void flush_init(std::thread::id id);

        void set_log_level(Severity severity) { log_level = severity; }

        void run();
        void finish();
    };

    void log(std::string&& msg, Severity severity = Severity::INFO, bool init = false);
    void flush();

    void trace(std::string&& msg, bool init = false);
    void debug(std::string&& msg, bool init = false);
    void info(std::string&& msg, bool init = false);
    void warning(std::string&& msg, bool init = false);
    void error(std::string&& msg, bool init = false);
}


#endif
