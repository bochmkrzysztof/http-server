#ifndef __SERVER_THREAD_H
#define __SERVER_THREAD_H


#include <boost/asio.hpp>

#include "scripts/path_manager.h"
#include "scripts/script_manager.h"
#include "scripts/response_manager.h"

#include "util/singleton.h"

#include <thread>
#include <latch>



namespace threads {
    class WorkerThread : public Singleton<const WorkerThread> {
    private:
        friend class Singleton<const WorkerThread>;

        std::thread handle;

        scripts::ScriptManager script_manager;
        scripts::PathManager path_manager;
        scripts::ResponseManager response_manager;

        WorkerThread();
        ~WorkerThread();

    public:
        auto get_path_manager() const -> const scripts::PathManager& { return path_manager; };
        auto get_response_manager() const -> const scripts::ResponseManager& { return response_manager; }

        static auto get_thread_response_manager() -> const scripts::ResponseManager& {
            return WorkerThread::get_thread_local_instance().get_response_manager();
        }
    };
}


#endif
