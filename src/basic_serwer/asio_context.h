#ifndef __ASIO_CONTEXT_H
#define __ASIO_CONTEXT_H


#include <boost/asio.hpp>

#include "util/singleton.h"


namespace io = boost::asio;


class AsioContext : public Singleton<AsioContext> {
private:
    friend class Singleton<AsioContext>;

    io::io_context ctx;     

    AsioContext() = default;
    ~AsioContext() = default;

public:
    auto get_ctx() -> io::io_context& { return ctx; }
};


#endif
