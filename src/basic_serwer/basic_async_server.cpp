#include "basic_async_server.h"
#include "basic_serwer/asio_context.h"

#include <fmt/core.h>

using namespace basic_server;

Server::Server(uint16_t port, ConnectionHandler connection_handler, ErrorHandler error_handler)
    :acceptor(AsioContext::get_instance().get_ctx(), tcp::endpoint(tcp::v4(), port)),
     connection_handler(connection_handler),
     error_handler(error_handler) {}
    
void Server::async_accept() {
    socket.emplace(AsioContext::get_instance().get_ctx());

    acceptor.async_accept(*socket, [&] (error_code error) {
        if(error) {
            error_handler(error);
        } else {
            connection_handler(std::move(*socket));   
            async_accept();
        }
    });
}

void Server::stop() {
    acceptor.close(); 
}
