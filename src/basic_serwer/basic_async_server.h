#ifndef __BASIC_ASYNC_SERVER_H
#define __BASIC_ASYNC_SERVER_H


#include <boost/asio.hpp>

#include <cstdint>
#include <optional>
#include <functional>



namespace io = boost::asio;
using tcp = io::ip::tcp;
using error_code = boost::system::error_code;


namespace basic_server {
    using ConnectionHandler = std::function<void(tcp::socket)>;
    using ErrorHandler = std::function<void(error_code)>;


    class Server {
    private:
        tcp::acceptor acceptor;

        std::optional<tcp::socket> socket;

        ConnectionHandler connection_handler;
        ErrorHandler error_handler;

    protected:
        Server(uint16_t port, ConnectionHandler connection_handler, ErrorHandler error_handler); 

        void async_accept();

    public:
        void stop();
    };
};

#endif
