#include "basic_serwer/serwer_thread.h"
#include "basic_serwer/asio_context.h"

#include "scripts/path_manager.h"
#include "scripts/response_manager.h"
#include "scripts/script_manager.h"

#include <memory>


using namespace threads;

WorkerThread::WorkerThread() 
    :script_manager(),
     response_manager(path_manager) {

    script_manager.load_files(&path_manager);     
}

WorkerThread::~WorkerThread() {
}
